<?php
//Silence
require_once __DIR__ . '/TrueScore.php';

$true = new TrueScore(
    array(
        'soundguys' => array(
            'score' => 7.3,
            'trust' => 0.8,
            'scale_type' => '1-to-10'
        ),
        'rtings' => array(
            'score' => 6.7,
            'trust' => 0.8,
            'scale_type' => '1-to-10'
        ),
        'headphones' => array(
            'score' => 3.4,
            'trust' => 0.4,
            'scale_type' => '1-to-5'
        ),
        'lifewire' => array(
            'score' => 4.2,
            'trust' => 0.8,
            'scale_type' => '1-to-5'
        ),
        'consumer_report' => array(
            'score' => 66,
            'trust' => 0.8,
            'scale_type' => '1-to-100'
        ),
        'which' => array(
            'score' => 57,
            'trust' => 0.8,
        ),
    ),
    array(
        'amazon' => array(
            'score' => 4.5,
            'scale_type' => '1-to-5',
        ),
        'walmart' => array(
            'score' => 4.7,
            'scale_type' => '1-to-5',
        ),
        'bestbuy' => array(
            'score' => 4.3,
            'scale_type' => '1-to-5',
        ),
    ),
    array(
        array(
            'expert' => 0.6,
            'customer' => 0.4,
        ),
        array(
            'expert' => 0.7,
            'customer' => 0.3,
        ),
        array(
            'expert' => 0.75,
            'customer' => 0.25,
        ),
        array(
            'expert' => 0.8,
            'customer' => 0.2,
        )
    )
);

var_dump($true->display_result());
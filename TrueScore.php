<?php

/**
 * The Algorithm to calculate true score.
 *
 * The initial true score calculation is our measure of factoring
 * in expert and customer scores in order to provide a weighted score
 * which reflects the overall data we have on a product / service.
 *
 * @author     Leobit <ddavydiyk@leobit.com>
 */
class TrueScore {

    /**
     * Experts data.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $experts    The array of experts data (score | trust index | scale type).
     */
    private $experts;

    /**
     * Customers data.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $customers    The array of customers data (score | scale type).
     */
    private $customers;

    /**
     * Rubric weight data.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $rubric_weight    The array of rubric weight data (expert index | customer index).
     */
    private $rubric_weight;

    /**
     * Weighted expert value.
     *
     * @since    1.0.0
     * @access   private
     * @var      float    $weighted_expert
     */
    private $weighted_expert;

    /**
     * Customers average.
     *
     * @since    1.0.0
     * @access   private
     * @var      float    $customers_avg
     */
    private $customers_avg;

    /**
     * True score.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $true_score
     */
    private $true_score;

    /**
     * Expert Score Average.
     *
     * @since    1.0.0
     * @access   private
     * @var      float    $expert_avg
     */
    private $expert_avg;

    /**
     * True Score vs Average Differential.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $true_vs_avg
     */
    private $true_vs_avg;

    /**
     * True Score vs Average Differential (duplicated).
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $true_vs_avg_dup
     */
    private $true_vs_avg_dup;

    /**
     * Clear converted experts values.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $converted_experts
     */
    private $converted_experts;

    /**
     * Clear converted customers values.
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $converted_customers
     */
    private $converted_customers;

    /**
     * Initialize the class and set its properties.
     *
     * @param array $experts
     * @param array $customers
     * @param array $rubric_weight
     * @since    1.0.0
     */
    public function __construct( array $experts, array $customers, array $rubric_weight ) {
        $this->experts = $experts;
        $this->customers = $customers;
        $this->rubric_weight = $rubric_weight;

        $this->init();
    }

    /**
     * Convert scale type from A to F to number.
     *
     * @param string $value
     * @return int
     * @since    1.0.0
     */
    public function toNumber(string $value) : int {
        if ($value) {
            return ord(strtolower($value)) - 96;
        } else {
            return 1;
        }
    }

    /**
     * Convert score to 100% based on scale type.
     *
     * @param string | int | float $score
     * @param string $scale_type
     * @return int
     * @since    1.0.0
     */
    public function convertScore( string|int|float $score, string $scale_type = '1-to-5') : int {
        return match ($scale_type) {
            '1-to-5' => (($score / 5) * 100),
            '1-to-10' => ($score / 10) * 100,
            '1-to-20' => round(($score / 20) * 100),
            'A-to-F' => round(($this->toNumber($score) / 6) * 100),
            default => round($score),
        };
    }

    /**
     * Calculate weighted expert.
     *
     * @since    1.0.0
     */
    private function setWeightedExpert() : void {
        $experts = array_filter($this->experts, fn($value) => !is_null($value['score']) && $value['score'] !== '');

        $sum = 0;
        $converted_experts = array();

        $sum_score = array_sum(array_map(function($item) use (&$sum, &$converted_experts) {
            $scale_type = $item['scale_type'] ?? '1-to-100';
            $score = $this->convertScore($item['score'], $scale_type);
            $converted_experts[] = $score;
            $sum += $score;

            return $score * $item['trust'];
        }, $experts));

        $this->expert_avg = $sum / count($experts);

        $sum_trust = array_sum(array_column($experts, 'trust'));

        $this->converted_experts = $converted_experts;
        $this->weighted_expert = $sum_score / $sum_trust;
    }

    /**
     * Calculate customer`s AVG.
     *
     * @since    1.0.0
     */
    private function setCustomerScore() : void {
        $customers = array_filter($this->customers);
        $converted_customers = array();

        $sum_score = array_sum(array_map(function($item) use(&$converted_customers) {
            $scale_type = $item['scale_type'] ?? '1_to_100';
            $score = $this->convertScore($item['score'], $scale_type);
            $converted_customers[] = $score;

            return $score;

        }, $customers));

        $average = $sum_score / count($customers);

        $this->customers_avg = $average;
        $this->converted_customers = $converted_customers;
    }

    /**
     * Calculate true score.
     *
     * @since    1.0.0
     */
    private function setTrueScore() : void {

        $true_score = array();

        for($i=0; $i < count($this->rubric_weight); $i++) {
            $true_score[] = $this->weighted_expert * $this->rubric_weight[$i]['expert'] + $this->customers_avg * $this->rubric_weight[$i]['customer'];
        }

        $this->true_score = $true_score;
    }

    /**
     * Calculate True Score vs Average Differential.
     *
     * @since    1.0.0
     */
    private function setTrueAvgDiff() : void {

        $true_vs_avg = array();
        $expert_avg = $this->getExpertAverage();
        $customer_avg = $this->getCustomerAverage();

        for($i=0; $i < count($this->true_score); $i++) {
            $true_vs_avg['customer'][] = ( abs( $this->true_score[$i] - $customer_avg ) / ( ( $this->true_score[$i] + $customer_avg ) / 2 ) ) * 100;
            $true_vs_avg['expert'][] = ( abs( $this->true_score[$i] - $expert_avg ) / ( ( $this->true_score[$i] + $expert_avg ) / 2 ) ) * 100;
        }

        $this->true_vs_avg = $true_vs_avg;
    }

    /**
     * Calculate True Score vs Average Differential.
     *
     * @since    1.0.0
     */
    private function setTrueAvgDiffDuplicated() : void {

        $true_vs_avg_dup = array();
        $expert_avg = $this->getExpertAverage();
        $customer_avg = $this->getCustomerAverage();
        $true_avg = array_sum($this->true_score) / count($this->true_score);

        $true_vs_avg_dup['customer'] = ( abs( $true_avg - $customer_avg ) / ( ( $true_avg + $customer_avg ) / 2 ) ) * 100;
        $true_vs_avg_dup['expert'] = ( abs( $true_avg - $expert_avg ) / ( ( $true_avg + $expert_avg ) / 2 ) ) * 100;

        $this->true_vs_avg_dup = $true_vs_avg_dup;
    }

    /**
     * Get true score.
     *
     * @since    1.0.0
     */
    public function getTrueScore() : array {

        return $this->true_score;
    }

    /**
     * Get true score.
     *
     * @since    1.0.0
     */
    public function getExpertWeighted() : float {

        return $this->weighted_expert;
    }

    /**
     * Get true score.
     *
     * @since    1.0.0
     */
    public function getCustomerAverage() : float {

        return $this->customers_avg;
    }

    /**
     * Get expert score avg.
     *
     * @since    1.0.0
     */
    public function getExpertAverage() : float {

        return $this->expert_avg;
    }

    /**
     * Get raw score.
     *
     * @since    1.0.0
     */
    public function getRawScore() : array {

        return array(
            'expert' => $this->getExpertAverage(),
            'customer' => $this->getCustomerAverage(),
            'combined' => ($this->getExpertAverage() + $this->getCustomerAverage()) / 2,
        );
    }

    /**
     * Get differential score.
     *
     * @since    1.0.0
     */
    public function getDiffScore() : float {
        return ( $this->getExpertWeighted() - $this->getExpertAverage() ) / $this->getExpertAverage() * 100;
    }

    /**
     * True Score vs Average Differential.
     *
     * @since    1.0.0
     */
    public function getTrueAvgDiff( string $key = 'customer') : array {
        return $this->true_vs_avg[$key];
    }

    /**
     * True Score vs Average Differential (Duplicated).
     *
     * @since    1.0.0
     */
    public function getTrueAvgDiffDuplicated( string $key = 'customer') : float {
        return $this->true_vs_avg_dup[$key];
    }

    /**
     * Get Median of clear array of customer or expert.
     *
     * @since    1.0.0
     */
    public function getMedian($key = 'customer') : float {

        $values = $this->converted_customers;
        if($key === 'expert') {
            $values = $this->converted_experts;
        }
        sort($values);

        $num = count($values);
        $middleVal = floor(($num - 1) / 2);

        if($num % 2) {
            return $values[$middleVal];
        } else {
            $lowMid = $values[$middleVal];
            $highMid = $values[$middleVal + 1];
            //Return the average of the low and high.
            return (($lowMid + $highMid) / 2);
        }
    }

    /**
     * Get Mode of clear array of customer or expert.
     *
     * @since    1.0.0
     */
    public function getMode($key = 'customer') : float {

        $values = $this->converted_customers;
        if($key === 'expert') {
            $values = $this->converted_experts;
        }

        $counted_arr = array_count_values($values);
        $mode = array_search(max($counted_arr), $counted_arr);

        return $mode;
    }

    /**
     * Get formatted result in one screen.
     *
     * @since    1.0.0
     */
    public function display_result() {
        return array(
            'customer_score_avg' => $this->getCustomerAverage(),
            'expert_score_avg' => $this->getExpertAverage(),
            'expert_weighted_avg' => $this->getExpertWeighted(),
            'true_score' => $this->getTrueScore(),
            'raw_score' => $this->getRawScore(),
            'differencial' => $this->getDiffScore(),
            'true_score_vs_avg_customer' => $this->getTrueAvgDiff('customer'),
            'true_score_vs_avg_expert' => $this->getTrueAvgDiff('expert'),
            'expert_mode' => $this->getMode('expert'),
            'customer_mode' => $this->getMode('customer'),
            'customer_median' => $this->getMedian('customer'),
            'expert_median' => $this->getMedian('expert'),
        );
    }

    public function init() {
        $this->setWeightedExpert();
        $this->setCustomerScore();
        $this->setTrueScore();
        $this->setTrueAvgDiff();
        $this->setTrueAvgDiffDuplicated();
    }
}
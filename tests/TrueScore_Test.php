<?php

require_once __DIR__ . '/../TrueScore.php';


/**
 * Test for True Score algorithm.
 *
 * The initial true score calculation is our measure of factoring
 * in expert and customer scores in order to provide a weighted score
 * which reflects the overall data we have on a product / service.
 *
 * @author     Leobit <ddavydiyk@leobit.com>
 */

class TrueScore_Test extends \PHPUnit\Framework\TestCase {

    private $algorithm;

    protected function setUp() : void {
        $this->algorithm = new TrueScore(
            array(
                'soundguys' => array(
                    'score' => 7.3,
                    'trust' => 0.8,
                    'scale_type' => '1-to-10'
                ),
                'rtings' => array(
                    'score' => 6.7,
                    'trust' => 0.8,
                    'scale_type' => '1-to-10'
                ),
                'headphones' => array(
                    'score' => 3.4,
                    'trust' => 0.4,
                    'scale_type' => '1-to-5'
                ),
                'lifewire' => array(
                    'score' => 4.2,
                    'trust' => 0.8,
                    'scale_type' => '1-to-5'
                ),
                'consumer_report' => array(
                    'score' => 66,
                    'trust' => 0.8,
                    'scale_type' => '1-to-100'
                ),
                'which' => array(
                    'score' => 57,
                    'trust' => 0.8,
                ),
            ),
            array(
                'amazon' => array(
                    'score' => 4.5,
                    'scale_type' => '1-to-5',
                ),
                'walmart' => array(
                    'score' => 4.7,
                    'scale_type' => '1-to-5',
                ),
                'bestbuy' => array(
                    'score' => 4.3,
                    'scale_type' => '1-to-5',
                ),
            ),
            array(
                array(
                    'expert' => 0.6,
                    'customer' => 0.4,
                ),
                array(
                    'expert' => 0.7,
                    'customer' => 0.3,
                ),
                array(
                    'expert' => 0.75,
                    'customer' => 0.25,
                ),
                array(
                    'expert' => 0.8,
                    'customer' => 0.2,
                )
            )
        );
        parent::setUp();
    }

    public function testGetCustomerAverage() : void {
        $this->assertSame( 90.0, $this->algorithm->getCustomerAverage() );
    }

    public function testGetTrueScore() : void {
        $this->assertSame( array(77.56363636363636, 75.4909090909091, 74.45454545454547, 73.41818181818184), $this->algorithm->getTrueScore() );
    }

    public function testGetExpertWeighted() : void {
        $this->assertSame( 69.27272727272728, $this->algorithm->getExpertWeighted() );
    }

    public function testGetExpertAverage() : void {
        $this->assertSame( 69.16666666666667, $this->algorithm->getExpertAverage() );
    }

    public function testGetRawScore() : void {
        $this->assertSame( array('expert' => 69.16666666666667, 'customer' => 90.0, 'combined' => 79.58333333333334), $this->algorithm->getRawScore() );
    }

    public function testGetDiffScore() : void {
        $this->assertSame( 0.15334063526835046, $this->algorithm->getDiffScore() );
    }

    public function testConvertScore() : void {
        $this->assertSame( 17, $this->algorithm->convertScore('A', 'A-to-F') );
        $this->assertSame( 60, $this->algorithm->convertScore(3, '1-to-5') );
        $this->assertSame( 40, $this->algorithm->convertScore(4, '1-to-10') );
        $this->assertSame( 50, $this->algorithm->convertScore(10, '1-to-20') );
        $this->assertSame( 46, $this->algorithm->convertScore(46, '1-to-100') );
    }

    public function testToNumber() : void {
        $this->assertSame( 1, $this->algorithm->toNumber('A') );
        $this->assertSame( 2, $this->algorithm->toNumber('B') );
        $this->assertSame( 3, $this->algorithm->toNumber('C') );
        $this->assertSame( 4, $this->algorithm->toNumber('D') );
        $this->assertSame( 5, $this->algorithm->toNumber('E') );
        $this->assertSame( 6, $this->algorithm->toNumber('F') );
    }

    public function testgetTrueAvgDiff() : void {
        $this->assertSame( array(14.84375, 17.534607778510207, 18.905472636815905, 20.29372496662214), $this->algorithm->getTrueAvgDiff('customer') );
        $this->assertSame( array(11.44544722331219, 8.743741751681089, 7.363645954214591, 5.963487981637744), $this->algorithm->getTrueAvgDiff('expert') );
    }

    public function testgetMode() : void {
        $this->assertSame( 90, $this->algorithm->getMode('customer') );
        $this->assertSame( 73, $this->algorithm->getMode('expert') );
    }

    public function testgetMedian() : void {
        $this->assertSame( 90, $this->algorithm->getMedian('customer') );
        $this->assertSame( 67.5, $this->algorithm->getMedian('expert') );
    }

}